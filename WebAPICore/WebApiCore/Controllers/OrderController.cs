﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiCore.Models;
using WebApiCore.ViewModels;

namespace WebApiCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly RestaurantDbContext _restaurantDb;

        public OrderController(RestaurantDbContext context)
        {
            _restaurantDb = context ?? throw new ArgumentNullException(nameof(context));
        }

        [HttpGet]  // api/order
        public async Task<ActionResult<IEnumerable<OrderVm>>> GetOrders()
        {
            return await _restaurantDb.Orders
                .Select(o => new OrderVm
                {
                    OrderID = o.OrderID,
                    OrderNo = o.OrderNo,
                    Customer = o.Customer.Name,
                    PMethod = o.PMethod,
                    GTotal = o.GTotal
                })
                .ToListAsync();
        }

        [HttpGet("{id}")]  // api/order/id
        public async Task<IActionResult> GetOrder(long id)
        {
            var order = await _restaurantDb.Orders
                           .Where(o => o.OrderID == id)
                           .Select(o => new
                           {
                               OrderID = o.OrderID,
                               o.OrderNo,
                               CustomerID = o.CustomerID,
                               o.PMethod,
                               o.GTotal,
                               DeleteOrderItemIDs = ""
                           })
                           .SingleAsync();

            var orderDetails = await _restaurantDb.OrderItems
                                 .Join(_restaurantDb.Items, oi => oi.ItemID, i => i.ItemID, (oi, i) => new { oi, i })
                                 .Where(oii => oii.oi.OrderID == id)
                                 .Select(oii => new
                                 {
                                     OrderID = oii.oi.OrderID,
                                     OrderItemID = oii.oi.OrderItemID,
                                     ItemID = oii.oi.ItemID,
                                     ItemName = oii.i.Name,
                                     oii.i.Price,
                                     oii.oi.Quantity,
                                     Total = oii.oi.Quantity * oii.i.Price
                                 })
                                 .ToListAsync();

            return Ok(new { order, orderDetails });
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrder(Order order)
        {
            try
            {
                //Order table
                if (order.OrderID == 0)
                {
                    _restaurantDb.Orders.Add(order);
                }
                else
                {
                    _restaurantDb.Entry(order).State = EntityState.Modified;
                }

                //OrderItems table
                foreach (var item in order.OrderItems)
                {
                    if (item.OrderItemID == 0)
                    {
                        _restaurantDb.OrderItems.Add(item);
                    }
                    else
                    {
                        _restaurantDb.Entry(item).State = EntityState.Modified;
                    }
                }

                //Delete for OrderItems
                if (!string.IsNullOrWhiteSpace(order.DeletedOrderItemIDs))
                {
                    foreach (var id in order.DeletedOrderItemIDs.Split(',').Where(x => x.Length != 0))
                    {
                        OrderItem x = _restaurantDb.OrderItems.Find(Convert.ToInt64(id));
                        _restaurantDb.OrderItems.Remove(x);
                    }
                }

                await _restaurantDb.SaveChangesAsync();

                order = await _restaurantDb.Orders.Where(o => o.OrderID == order.OrderID).Include(o => o.Customer).SingleAsync();
                return CreatedAtAction(nameof(GetOrder), new { id = order.OrderID }, CreateOrderVm(order));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder(int id)
        {
            Order order = _restaurantDb.Orders
                .Include(o => o.OrderItems)
                .SingleOrDefault(x => x.OrderID == id);
            if (order == null)
            {
                return NotFound();
            }

            foreach (var item in order.OrderItems.ToList())
            {
                _restaurantDb.OrderItems.Remove(item);
            }

            _restaurantDb.Orders.Remove(order);
            await _restaurantDb.SaveChangesAsync();
            order.OrderItems = null;
            return Ok(order);
        }

        private OrderVm CreateOrderVm(Order o)
            => new OrderVm
            {
                OrderID = o.OrderID,
                OrderNo = o.OrderNo,
                Customer = o.Customer.Name,
                PMethod = o.PMethod,
                GTotal = o.GTotal
            };
    }
}
