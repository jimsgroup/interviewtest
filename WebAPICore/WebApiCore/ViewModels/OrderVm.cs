﻿namespace WebApiCore.ViewModels
{
    public class OrderVm
    {
        public long OrderID { get; set; }
        public string OrderNo { get; set; }
        public string Customer { get; set; }
        public string PMethod { get; set; }
        public decimal? GTotal { get; set; }
    }
}
