﻿using System.Collections.Generic;

namespace WebApiCore.Models
{
    public class Item
    {
        public int ItemID { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; } = new HashSet<OrderItem>();
    }
}