﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApiCore.Models
{
    public class Order
    {
        public long OrderID { get; set; }
        public string OrderNo { get; set; }
        public string PMethod { get; set; }
        public decimal? GTotal { get; set; }

        public int? CustomerID { get; set; }
        public virtual Customer Customer { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; } = new HashSet<OrderItem>();

        [NotMapped]
        public string DeletedOrderItemIDs { get; set; }
    }
}
