﻿using System.Collections.Generic;

namespace WebApiCore.Models
{
    public class Customer
    {
        public int CustomerID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Order> Orders { get; set; } = new HashSet<Order>();

    }
}
