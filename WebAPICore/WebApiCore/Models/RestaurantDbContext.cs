﻿using Microsoft.EntityFrameworkCore;
using System;

namespace WebApiCore.Models
{
    public class RestaurantDbContext : DbContext
    {
        public RestaurantDbContext(DbContextOptions<RestaurantDbContext> options)
            : base(options)
        {
            // no implementation needed
        }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderItem> OrderItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var builder = modelBuilder ?? throw new ArgumentNullException(nameof(modelBuilder));
            builder.HasDefaultSchema("dbo");
            
            builder.Entity<Customer>()
                .ToTable("Customer")
                .Property(c => c.CustomerID).HasColumnName("CustomerID")
                .ValueGeneratedOnAdd();

            builder.Entity<Item>()
                .ToTable("Item")
                .Property(i => i.ItemID).HasColumnName("ItemID")
                .ValueGeneratedOnAdd();
            builder.Entity<Item>()
                .Property(i => i.Price)
                .HasColumnType("decimal(18,2)");

            builder.Entity<Order>()
                .ToTable("Order")
                .Property(o => o.OrderID).HasColumnName("OrderID")
                .ValueGeneratedOnAdd();
            builder.Entity<Order>()
                .HasOne(o => o.Customer)
                .WithMany(c => c.Orders)
                .HasForeignKey(o => o.CustomerID);
            builder.Entity<Order>()
                .Property(o => o.GTotal)
                .HasColumnType("decimal(18,2)");

            builder.Entity<OrderItem>()
                .ToTable("OrderItems")
                .Property(oi => oi.OrderItemID).HasColumnName("OrderItemID")
                .ValueGeneratedOnAdd();
            builder.Entity<OrderItem>()
                .HasOne(oi => oi.Item)
                .WithMany(i => i.OrderItems)
                .HasForeignKey(oi => oi.ItemID);
            builder.Entity<OrderItem>()
                .HasOne(oi => oi.Order)
                .WithMany(o => o.OrderItems)
                .HasForeignKey(oi => oi.OrderID);
        }
    }
}
