﻿namespace WebApiCore.Models
{
    public class OrderItem
    {
        public long OrderItemID { get; set; }
        public int? Quantity { get; set; }

        public int? ItemID { get; set; }
        public virtual Item Item { get; set; }

        public long? OrderID { get; set; }
        public virtual Order Order { get; set; }
    }
}